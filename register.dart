import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:module_3/edit.dart';
import 'package:module_3/login.dart';
import 'package:module_3/main.dart';

class Second extends StatelessWidget {
  const Second({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text("Register"),
          centerTitle: true,
        ),
        body: Column(
          children: [
            SizedBox(
              height: 80,
              width: 80,
              child: Stack(),
            ),
            Padding(
              padding: const EdgeInsets.all(6.0),
              child: const TextField(
                obscureText: true,
                decoration: InputDecoration(
                    border: OutlineInputBorder(), labelText: "Name"),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(6.0),
              child: const TextField(
                obscureText: true,
                decoration: InputDecoration(
                    border: OutlineInputBorder(), labelText: "Surname"),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(6.0),
              child: const TextField(
                obscureText: true,
                decoration: InputDecoration(
                    border: OutlineInputBorder(), labelText: "Email Address"),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(6.0),
              child: const TextField(
                obscureText: true,
                decoration: InputDecoration(
                    border: OutlineInputBorder(), labelText: "Username"),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(6.0),
              child: const TextField(
                obscureText: true,
                decoration: InputDecoration(
                    border: OutlineInputBorder(), labelText: "Password"),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(6.0),
              child: const TextField(
                obscureText: true,
                decoration: InputDecoration(
                    border: OutlineInputBorder(), labelText: "Date of Birth"),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(6.0),
              child: const TextField(
                obscureText: true,
                decoration: InputDecoration(
                    border: OutlineInputBorder(), labelText: "Gender"),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(6.0),
              child: const TextField(
                obscureText: true,
                decoration: InputDecoration(
                    border: OutlineInputBorder(), labelText: "Phone Number"),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: ElevatedButton(
                onPressed: () => {
                  Navigator.pop(
                      context, MaterialPageRoute(builder: (context) => First()))
                },
                child: const Text("Submit"),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
